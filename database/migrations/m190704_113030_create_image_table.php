<?php

use app\modules\base\db\Migration;

/**
 * Handles the creation of table `{{%image}}`.
 */
class m190704_113030_create_image_table extends Migration
{
	/**
	 * @inheritdoc
	 */
	public function up()
	{
		$this->createTable('{{%image}}', [
			'id'         => $this->primaryKey(),
            'product_id' => $this->integer()->notNull(),
            'name'       => $this->string()->notNull(),
		], $this->tableOptions());

        $this->addForeignKey(
            'fk_image_product_id',
            'image',
            'product_id',
            'product',
            'id',
            'CASCADE'
        );
	}

	/**
	 * @inheritdoc
	 */
	public function down()
	{
        $this->dropForeignKey(
            'fk_image_product_id',
            'image'
        );

		$this->dropTable('{{%image}}');
	}
}
