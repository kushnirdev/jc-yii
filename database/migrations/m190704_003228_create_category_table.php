<?php

use app\modules\base\db\Migration;

/**
 * Handles the creation of table `{{%category}}`.
 */
class m190704_003228_create_category_table extends Migration
{
	/**
	 * @inheritdoc
	 */
	public function up()
	{
		$this->createTable('{{%category}}', [
			'id' => $this->primaryKey(),
            'parent_id' => $this->integer()->notNull(),
            'name'  => $this->string()->notNull()->unique(),
		], $this->tableOptions());

        $this->addForeignKey(
            'fk_category_parent_id',
            'category',
            'parent_id',
            'category',
            'id',
            'CASCADE'
        );
	}

	/**
	 * @inheritdoc
	 */
	public function down()
	{
        $this->dropForeignKey(
            'fk_category_parent_id',
            'category'
        );

		$this->dropTable('{{%category}}');
	}
}
