<?php

use app\modules\base\db\Migration;

/**
 * Handles the creation of table `{{%product_category}}`.
 */
class m190704_102922_create_product_category_table extends Migration
{
	/**
	 * @inheritdoc
	 */
	public function up()
	{
		$this->createTable('{{%product_category}}', [
			'id' => $this->primaryKey(),
            'product_id' => $this->integer()->notNull(),
            'category_id' => $this->integer()->notNull(),
		], $this->tableOptions());

        $this->addForeignKey(
            'fk_product_category_product_id',
            'product_category',
            'product_id',
            'product',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_product_category_category_id',
            'product_category',
            'category_id',
            'category',
            'id',
            'CASCADE'
        );
	}

	/**
	 * @inheritdoc
	 */
	public function down()
	{
        $this->dropForeignKey(
            'fk_product_category_product_id',
            'product_category'
        );
        $this->dropForeignKey(
            'fk_product_category_category_id',
            'product_category'
        );

		$this->dropTable('{{%product_category}}');
	}
}
