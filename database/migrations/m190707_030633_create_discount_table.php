<?php

use app\modules\base\db\Migration;

/**
 * Handles the creation of table `{{%discount}}`.
 */
class m190707_030633_create_discount_table extends Migration
{
	/**
	 * @inheritdoc
	 */
	public function up()
	{
		$this->createTable('{{%discount}}', [
			'id'         => $this->primaryKey(),
            'product_id' => $this->integer()->notNull(),
            'quantity'   => $this->integer()->notNull(),
            'discount'   => $this->integer()->notNull(),
        ], $this->tableOptions());

        $this->addForeignKey(
            'fk_discount_product_id',
            'discount',
            'product_id',
            'product',
            'id',
            'CASCADE'
        );
	}

	/**
	 * @inheritdoc
	 */
	public function down()
	{
        $this->dropForeignKey(
            'fk_discount_product_id',
            'discount'
        );

		$this->dropTable('{{%discount}}');
	}
}
