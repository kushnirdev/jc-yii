<?php

use app\modules\base\db\Migration;

/**
 * Handles the creation of table `{{%product}}`.
 */
class m190704_002350_create_product_table extends Migration
{
	/**
	 * @inheritdoc
	 */
	public function up()
	{
		$this->createTable('{{%product}}', [
			'id'        => $this->primaryKey(),
            'name'  => $this->string()->notNull()->unique(),
            'quantity'   => $this->integer()->notNull(),
            'discount'   => $this->integer(),
		], $this->tableOptions());
	}

	/**
	 * @inheritdoc
	 */
	public function down()
	{
		$this->dropTable('{{%product}}');
	}
}
