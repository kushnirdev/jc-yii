<?php

namespace app\fixtures;

class CategoryFixture extends ActiveFixture
{
	public $modelClass = '\\app\\models\\Category';
}