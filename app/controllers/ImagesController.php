<?php

namespace app\controllers;

use app\models\Image;
use yii\filters\VerbFilter;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ImagesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Deletes an existing Image model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $model = Image::findOne($id);
        if ($model->delete()) {
            $dir = \Yii::getAlias('@productImages') . "$model->product_id/";
            if (is_dir($dir)) {
                unlink($dir.$model->name);
            }        }
        return $this->redirect(['products/view', 'id' => $model->product_id]);
    }
}
