<?php

namespace app\controllers;

use app\models\Model;
use app\models\ProductCategory;
use app\models\search\ProductSearch;
use Yii;
use app\models\Discount;
use app\models\Product;
use app\controllers\Controller;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * DiscountsController implements the CRUD actions for Discount model.
 */
class DiscountsController extends Controller
{
    /**
     * Lists all Discount models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Updates an existing Discount model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = Product::findOne($id);
        $modelProductDiscounts = $model->discounts;

        if (Yii::$app->request->post()) {
            $oldIDs = ArrayHelper::map($modelProductDiscounts, 'id', 'id');

            $modelProductDiscounts = Model::createMultiple(Discount::class, $modelProductDiscounts);
            Model::loadMultiple($modelProductDiscounts, Yii::$app->request->post());

            $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelProductDiscounts, 'id', 'id')));

            foreach ($modelProductDiscounts as $modelProductDiscount) {
                if (!$modelProductDiscount->quantity && !$modelProductDiscount->discount) {
                    $deletedIDs[] = $modelProductDiscount->id;
                }
            }
            // ajax validation
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ArrayHelper::merge(
                    ActiveForm::validateMultiple($modelProductDiscounts),
                    ActiveForm::validate($model)
                );
            }

            // validate all models
            $valid = Model::validateMultiple($modelProductDiscounts);

            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {
                        if (! empty($deletedIDs)) {
                            Discount::deleteAll(['id' => $deletedIDs]);
                        }
                        if ($modelProductDiscounts) {
                            foreach ($modelProductDiscounts as $modelProductDiscount) {
                                $modelProductDiscount->product_id = $model->id;
                                if (!($flag = $modelProductDiscount->save())) {
                                    $transaction->rollBack();
                                    break;
                                }
                            }
                        }
                    }
                    if ($flag) {
                        $transaction->commit();
                        return $this->redirect(['products/view', 'id' => $model->id]);
                    }
                } catch (\Exception $e) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
            'modelProductDiscounts' => empty($modelProductDiscounts) ? [new Discount] : $modelProductDiscounts,
        ]);
    }
}
