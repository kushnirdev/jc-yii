<?php

namespace app\assets;

class AssetBundle extends \yii\web\AssetBundle
{
	public $sourcePath = '@app/assets';

	public $css = [
		'css/site.css',
	];
	public $js = [
        '//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js',
    ];
	public $depends = [
		'yii\web\YiiAsset',
		'yii\bootstrap\BootstrapAsset',
	];

    public $jsOptions = array(
        'position' => \yii\web\View::POS_HEAD
    );
}
