<?php

use app\models\Product;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Product */
/* @var $form yii\widgets\ActiveForm */
/* @var array $modelProductDiscounts */
?>

<div class="discount-form">

    <?php $form = ActiveForm::begin([
        'id' => 'dynamic-form',
    ]); ?>

    <div class="panel panel-default">
        <div class="panel-heading"><h4>Discounts</h4></div>
        <div class="panel-body">
            <?php
            /** @var array $modelProductDiscounts */
            \wbraganca\dynamicform\DynamicFormWidget::begin([
                'widgetContainer' => 'dynamicform_wrapper_discount', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                'widgetBody' => '.container-items', // required: css class selector
                'widgetItem' => '.item', // required: css class
                'limit' => 999, // the maximum times, an element can be cloned (default 999)
                'min' => 1, // 0 or 1 (default 1)
                'insertButton' => '.add-item', // css class
                'deleteButton' => '.remove-item', // css class
                'model' => $modelProductDiscounts[0],
                'formId' => 'dynamic-form',
                'formFields' => [
                    'quantity',
                    'discount',
                ],
            ]); ?>

            <div class="container-items"><!-- widgetContainer -->
                <?php foreach ($modelProductDiscounts as $i => $modelProductDiscount): ?>
                    <div class="item panel panel-default"><!-- widgetBody -->
                        <div class="panel-heading">
                            <h3 class="panel-title pull-left">Discount</h3>
                            <div class="pull-right">
                                <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                                <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <?php
                            // necessary for update action.
                            if (!$modelProductDiscount->isNewRecord) {
                                echo Html::activeHiddenInput($modelProductDiscount, "[{$i}]id");
                            } ?>
                            <div class="row">
                                <div class="col-sm-6">
                                    <?= $form->field($modelProductDiscount, "[{$i}]quantity")->input('number', ['maxlength' => true]) ?>
                                </div>
                                <div class="col-sm-6">
                                    <?= $form->field($modelProductDiscount, "[{$i}]discount")->input('number', ['maxlength' => true]) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <?php \wbraganca\dynamicform\DynamicFormWidget::end(); ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$this->registerJs('
    $(".dynamicform_wrapper_discount").on("beforeInsert", function(e, item) {
    console.log("beforeInsert");
});

$(".dynamicform_wrapper_discount").on("afterInsert", function(e, item) {
    console.log("afterInsert");
});

$(".dynamicform_wrapper_discount").on("beforeDelete", function(e, item) {
    if (! confirm("Are you sure you want to delete this item?")) {
        return false;
    }
    return true;
});

$(".dynamicform_wrapper_discount").on("afterDelete", function(e) {
    console.log("Deleted item!");
});

$(".dynamicform_wrapper_discount").on("limitReached", function(e, item) {
    alert("Limit reached");
});
')?>