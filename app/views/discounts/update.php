<?php

use app\models\Product;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Product */
/** @var array $modelProductDiscounts */

$this->title = 'Assign Discounts to Product: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Discounts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="discount-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelProductDiscounts' => $modelProductDiscounts,
    ]) ?>

</div>
