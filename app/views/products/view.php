<?php

use yii\helpers\BaseStringHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;
use app\assets\AssetBundle;

/* @var $this yii\web\View */
/* @var $model app\models\Product */


$assets = AssetBundle::register($this);

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="product-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'quantity',
            [
                'label'     => Html::a(\Yii::t('app', $model->getAttributeLabel('discount')), ['discounts/update', 'id' => $model->id]),
                'attribute' => 'discount',
                'value'     => function ($model) {
                    return $model->discount ? $model->discount : 0;
                }
            ],
        ],
    ]) ?>

<!--    Image block    -->
    <div class="row">
        <?php
            /** @var \app\models\Image $images */
            foreach ($images as $image) {
        ?>
        <div class="col-md-3">
            <div class="card">

                <div class="zoom-gallery">
                    <a href="<?= "$assets->baseUrl/images/products/$model->id/$image->name" ?>" style="width: 100%;">
                        <img id="map" src="<?= "$assets->baseUrl/images/products/$model->id/$image->name" ?>" width="200" height="200">
                    </a>
                    <?= Html::a(
                        \Yii::t('app', 'Delete Image'),
                        ['images/delete', 'id' => $image->id],
                        ['class' => 'btn left-space btn-danger','data' => [
                            'method' => 'post',
                            ],
                        ]
                    ); ?>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
</div>