<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/** @var array $modelProductCategories */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="panel panel-default">
    <div class="panel-heading"><h4>Categories</h4></div>
    <div class="panel-body">
        <?php
        /** @var array $modelProductCategories */


        \wbraganca\dynamicform\DynamicFormWidget::begin([
            'widgetContainer' => 'dynamicform_wrapper_category', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
            'widgetBody' => '.container-items', // required: css class selector
            'widgetItem' => '.item', // required: css class
            'limit' => 4, // the maximum times, an element can be cloned (default 999)
            'min' => 1, // 0 or 1 (default 1)
            'insertButton' => '.add-item', // css class
            'deleteButton' => '.remove-item', // css class
            'model' => $modelProductCategories[0],
            'formId' => 'dynamic-form',
            'formFields' => [
                'category_id',
            ],
        ]); ?>

        <div class="container-items"><!-- widgetContainer -->
            <?php foreach ($modelProductCategories as $i => $modelProductCategory): ?>
                <div class="item panel panel-default"><!-- widgetBody -->
                    <div class="panel-heading">
                        <h3 class="panel-title pull-left">Category</h3>
                        <div class="pull-right">
                            <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                            <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <?php
                        // necessary for update action.
                        if (!$modelProductCategory->isNewRecord) {
                            echo Html::activeHiddenInput($modelProductCategory, "[{$i}]id");
                        } ?>
                        <div class="row">
                            <div class="col-sm-6">
                                <?= $form->field($modelProductCategory, "[{$i}]category_id")->dropDownList(app\models\Category::getSelectForProduct(), [
                                    'prompt' => 'without category',
                                    'class'  => 'form-control form-field-short',
                                ]); ?>
                            </div>
                        </div><!-- .row -->
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <?php \wbraganca\dynamicform\DynamicFormWidget::end(); ?>
    </div>
</div>
<?php
$this->registerJs('
    $(".dynamicform_wrapper_category").on("beforeInsert", function(e, item) {
    console.log("beforeInsert");
});

$(".dynamicform_wrapper_category").on("afterInsert", function(e, item) {
    console.log("afterInsert");
});

$(".dynamicform_wrapper_category").on("beforeDelete", function(e, item) {
    if (! confirm("Are you sure you want to delete this item?")) {
        return false;
    }
    return true;
});

$(".dynamicform_wrapper_category").on("afterDelete", function(e) {
    console.log("Deleted item!");
});

$(".dynamicform_wrapper_category").on("limitReached", function(e, item) {
    alert("Limit reached");
});
')?>