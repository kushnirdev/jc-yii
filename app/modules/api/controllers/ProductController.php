<?php

namespace app\modules\api\controllers;

use app\models\Product;

class ProductController extends \yii\web\Controller
{
    /**
     * @param int $id
     *
     * @return string Json
     * @throws \yii\web\HttpException
     */
    public function actionGetProductDetails($id)
    {
        $result = $this->getProductData($id);
        if ($result) {
            return \yii\helpers\Json::encode($result);
        }

        // data does not exist
        throw new \yii\web\HttpException(404, 'The requested Item could not be found.');
    }

    /**
     * @param int $id
     *
     * @return array|null
     */
    private function getProductData($id)
    {
        $data = Product::find()
            ->where('id =:pid', [':pid' => $id])
            ->with('productCategories')
            ->with('images')
            ->with('discounts')
            ->asArray()
            ->one();

        return $data;
    }

}

