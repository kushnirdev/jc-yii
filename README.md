<p align="center">
    <a href="https://github.com/yiisoft" target="_blank">
        <img src="https://avatars0.githubusercontent.com/u/993323" height="100px">
    </a>
    <h1 align="center">Yii 2 JustCoded Test Project</h1>
    <br>
</p>

DIRECTORY STRUCTURE
-------------------

      app/                contains your application classes and files
        |-- assets           css/js assets and AssetBundle classes
        |-- base             base classes (overwrite yii\base or few more) 
        |-- components       application "components" classes 
        |-- console          controllers for console commands
        |-- controllers      controllers for web application
        |-- filters          application filters (similar to yii\filters) 
        |-- forms            various form models 
        |-- mail             view files for e-mails 
        |-- models           ActiveRecord model classes 
        |-- modules          connected modules, admin panel module by default
        |-- rbac             RBAC Manager / components 
        |-- traits           global traits, grouped by type 
        |-- views            view files for the Web application
        |-- widgets          application widgets to use inside views 
      config/             contains application configurations
      database/           contains migration and fixtures
      tests/              contains various tests for the basic application
      vendor/             contains dependent 3rd-party packages
      public/             contains public assets and web entry script index.php / server docroot

REQUIREMENTS
------------

The minimum requirement by this project template that your Web server supports PHP 7.0.


INSTALLATION
------------

### Install via Composer
Clone project from

```
https://gitlab.com/kushnirdev/jc-yii.git
```


```
composer install
```

CONFIGURATION
-------------

### ENV support

Config files are the same for all environments. You don't need to create some "local" config files.
Instead you can accept different parameters from server environment with `env()` helper function. 

Server environment variables can be set through web server vhost configuration, .htaccess file, 
or .env file in project root (the simplest option).

To start using the project template copy .env.example as .env in the project root and setup it.

### Web
Copy `/public/.htaccess.example` as `/public/.htaccess` to enable pretty URLs support and cache/expire 
tokens required by Google PageSpeed Insights test.

Furthermore you should check such options inside .env:

```php
APP_ENV=dev
APP_DEBUG=true
APP_KEY=wUZvVVKJyHFGDB9qK_Lop4QE1vwb4bYU
```

*`APP_KEY` is used as cookie verification key. Unfortunately there are no post install composer script to generate it automatically*

### Database

You should update your .env file config:

```php
DB_HOST=127.0.0.1
DB_NAME=yii2_starter
DB_USER=root
DB_PASS=12345
```

**NOTES:**
- Yii won't create the database for you, this has to be done manually before you can access it.

LAUNCH
-------

You will need to create required tables through migrations and init RBAC extension.
Launch the commands below from terminal:

```bash
php yii migrate
php yii fixture/load User
php yii fixture/load Category
php yii rbac/init
php yii rbac/assign-master 1
php yii rbac/scan
php yii rbac/scan --path=@vendor/justcoded/yii2-rbac/ --routesBase=admin/rbac/
```

Now you should be able to access the application through the following URL, assuming `my-project` is the directory
directly under the Web root.

	http://localhost/my-project/public/

Or you can run `yii serve` to launch Yii built-in web server, similar to usual Yii basic application.

You should login as admin:

	User:       admin@domain.com
	Password:   password_0
